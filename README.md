# WeatherApp z Mockami
Przykłady przeprowadzenia testów za pomocą mocków (atrap) w 4 ujęciach.

Ten plik zbiera komentarze, które poczyniłek w każdym z wariantów.

### MainBardzoZły
Tutaj jest ten najgorszy element - żeby podłożyć atrapę pod clienta, trzeba było zdjąć modyfikator "final".

### MainZły
Wersja nieco lepsza, bo można podmienić wykonanie metody getClient, żeby zmienić obiekt, z którego korzysta implementacja - nie trzeba usuwać słowa final z prawdziwego kodu. To poważnie zmienia resztę kodu, bo nie można zamockować statycznych metod.

Dygresja: To znaczy nie można przy pomocy Mockito, da się przy pomocy PowerMocka, ale nie należy pisać kodu, który potrzebuje PowerMocka. To narzędzie do ratowania stareg kodu.

### MainNiezły
Ulepszona wersja "zła" - współdzielony klient jest przekazywany jako parametr do konstruktora klasy, którą będziemy wywoływać zapytanie - unikamy szpiegów.

Jest to kanoniczne podejście do przekazywania zależności w programowaniu obiektowym. Można podejść lepiej, jeśli użyje się przekazywanych funkcji, gdzie "lepiej" jest rozumiane jako "mniej mocków".

### MainLepszy
Wersja "trochę lepsza" od pozostałych, bo potrzebuje 3x mniej atrap, żeby dało się ją przetestować. Zastępujemy funkcję z 4. argumentu naszą własną, która zwraca nam spreparowaną odpowiedź. Dzięki temu nie musimy mockować całej interakcji z AsyncHttpClient - nasza nowa funkcja leży na punktach styku między kodem naszej aplikacji a interakcją z zewnątrznym bytem, jakim jest klient.

Wciąż potrzebujemy mocka obiektu odpowiedzi, ponieważ interfejs Response jest bardzo rozbudowany, a nas interesują tylko dwie metody, nad którymi chcemy mieć kontrolę.
