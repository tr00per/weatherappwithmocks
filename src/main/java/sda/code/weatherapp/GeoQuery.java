package sda.code.weatherapp;

import org.asynchttpclient.RequestBuilder;

public class GeoQuery implements Query {
    private final double lat;
    private final double lon;

    public GeoQuery(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    @Override
    public void applyToBuilder(RequestBuilder builder) {
        builder.addQueryParam("lat", String.valueOf(lat))
                .addQueryParam("lon", String.valueOf(lon));
    }
}
