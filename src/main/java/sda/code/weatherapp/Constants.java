package sda.code.weatherapp;

public class Constants {
    public static final String API_KEY = "901a7cbab7c2ae78d0ee3ff6ca543f47";
    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/weather";

    public static final String OW_BASE = "http://api.openweathermap.org";
    public static final String OW_WEATHER = "/data/2.5/weather";
}
