package sda.code.weatherapp.retrofit;

import sda.code.weatherapp.model.WeatherModel;

import java.io.IOException;
import java.util.Optional;

public interface Query {
    Optional<WeatherModel> execute(OpenWeather api, String apiKey) throws IOException;
}
