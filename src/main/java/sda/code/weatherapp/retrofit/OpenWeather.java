package sda.code.weatherapp.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import sda.code.weatherapp.Constants;
import sda.code.weatherapp.model.WeatherModel;

public interface OpenWeather {
    // Query params nie można przekazywać za pomocą argumentów adnotowanych @Path.
    // Retrofit zgłosi o to pretensje podczas próby stworzenia takiego interfejsu,
    // naprowadzając nas tym samym na prawiłowe użycie biblioteki.
    @GET("/data/{version}/weather")
    Call<WeatherModel> weatherByQuery(
            @Path("version") String version,
            @Query("key") String apiKey,
            @Query("query") String query
    );

    @GET("/data/2.5/weather")
    Call<WeatherModel> weatherByCoord(
            @Query("appid") String apiKey,
            @Query("lat") double latitude,
            @Query("lon") double longitude
    );

    @GET(Constants.OW_WEATHER)
    Call<WeatherModel> weather(
            @Query("appid") String apiKey,
            @Query("q") String query,
            @Query("lat") Double latitude,
            @Query("lon") Double longitude,
            @Query("units") String units,
            @Query("lang") String langCode
    );
}
