package sda.code.weatherapp.retrofit;

import io.reactivex.Observable;
import sda.code.weatherapp.model.WeatherModel;

import static java.util.Objects.requireNonNull;

public class Rx2QueryBuilder {
    private final Rx2OpenWeather api;
    private final String apiKey;
    private GeoQuery geoQuery;
    private CityQuery cityQuery;
    private String units = "metric";
    private String langCode = "pl";

    public Rx2QueryBuilder(Rx2OpenWeather api, String apiKey) {
        this.api = requireNonNull(api);
        this.apiKey = requireNonNull(apiKey);
    }

    public Rx2QueryBuilder withQuery(GeoQuery geoQuery) {
        this.geoQuery = requireNonNull(geoQuery);
        return this;
    }

    public Rx2QueryBuilder withQuery(CityQuery cityQuery) {
        this.cityQuery = requireNonNull(cityQuery);
        return this;
    }

    public Rx2QueryBuilder withUnits(String units) {
        this.units = requireNonNull(units);
        return this;
    }

    public Rx2QueryBuilder withLangCode(String langCode) {
        this.langCode = requireNonNull(langCode);
        return this;
    }

    void validate() {
        if (cityQuery != null && geoQuery != null) {
            throw new IllegalStateException("Cannot pass both queries at once");
        }
        if (cityQuery == null && geoQuery == null) {
            throw new IllegalStateException("Must pass at least one query");
        }
    }

    public Observable<WeatherModel> build() {
        validate();
        return api.weather(
                apiKey,
                cityQuery != null ? cityQuery.toString() : null,
                geoQuery != null ? geoQuery.getLat() : null,
                geoQuery != null ? geoQuery.getLon() : null,
                units,
                langCode
        );
    }
}
