package sda.code.weatherapp.retrofit;

import lombok.Data;
import retrofit2.Response;
import sda.code.weatherapp.model.WeatherModel;

import java.io.IOException;
import java.util.Optional;

@Data
public class CityQuery implements Query {
    private final String city;
    private final String code;

    public CityQuery(String city) {
        this.city = city;
        this.code = null;
    }

    public CityQuery(String city, String code) {
        this.city = city;
        this.code = code;
    }

    @Override
    public String toString() {
        return city + (code != null ? "," + code : "");
    }


    // To jest fragment z pierwszego podejścia do Retrofita, co do zasady nieprawidłowego,
    // bo nasz "piękny, czysty" obiekt na dane wejściowe nagle dowiaduje się, co będzię z nim się działo.
    // Obiekt trzymający dane nie powinien interiesować się tym, w jakim kontekście będzie używany.
    @Override
    public Optional<WeatherModel> execute(OpenWeather api, String apiKey) throws IOException {
        Response<WeatherModel> response = api.weatherByQuery("2.5", apiKey, toString()).execute();
        if (response.isSuccessful()) {
            return Optional.ofNullable(response.body());
        } else {
            System.err.println(response.errorBody());
            return Optional.empty();
        }
    }
}
