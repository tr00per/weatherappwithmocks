package sda.code.weatherapp.retrofit;

import lombok.Data;
import retrofit2.Response;
import sda.code.weatherapp.model.WeatherModel;

import java.io.IOException;
import java.util.Optional;

@Data
public class GeoQuery implements Query {
    private final double lat;
    private final double lon;

    public GeoQuery(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    // To jest fragment z pierwszego podejścia do Retrofita, co do zasady nieprawidłowego,
    // bo nasz "piękny, czysty" obiekt na dane wejściowe nagle dowiaduje się, co będzię z nim się działo.
    // Obiekt trzymający dane nie powinien interiesować się tym, w jakim kontekście będzie używany.
    @Override
    public Optional<WeatherModel> execute(OpenWeather api, String apiKey) throws IOException {
        Response<WeatherModel> response = api.weatherByCoord(apiKey, lat, lon).execute();
        if (response.isSuccessful()) {
            return Optional.ofNullable(response.body());
        } else {
            System.err.println(response.errorBody());
            return Optional.empty();
        }
    }
}
