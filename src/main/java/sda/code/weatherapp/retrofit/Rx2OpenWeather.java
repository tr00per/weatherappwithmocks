package sda.code.weatherapp.retrofit;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import sda.code.weatherapp.Constants;
import sda.code.weatherapp.model.WeatherModel;

public interface Rx2OpenWeather {
    @GET("/data/2.5/weather?appid={key}&q={query}")
    Observable<WeatherModel> weatherByQuery(
            @Path("key") String apiKey,
            @Path("query") String query
    );

    @GET("/data/2.5/weather")
    Observable<WeatherModel> weatherByCoord(
            @Query("appid") String apiKey,
            @Query("lat") double latitude,
            @Query("lon") double longitude
    );

    @GET(Constants.OW_WEATHER)
    Observable<WeatherModel> weather(
            @Query("appid") String apiKey,
            @Query("q") String query,
            @Query("lat") Double latitude,
            @Query("lon") Double longitude,
            @Query("units") String units,
            @Query("lang") String langCode
    );
}
