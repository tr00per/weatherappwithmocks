package sda.code.weatherapp;

import org.asynchttpclient.RequestBuilder;

public class CityQuery implements Query {
    private final String city;
    private final String code;

    public CityQuery(String city) {
        this.city = city;
        this.code = null;
    }

    public CityQuery(String city, String code) {
        this.city = city;
        this.code = code;
    }

    @Override
    public String toString() {
        return city + (code != null ? "," + code : "");
    }

    @Override
    public void applyToBuilder(RequestBuilder builder) {
        builder.addQueryParam("q", toString());
    }
}
