package sda.code.weatherapp;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sda.code.weatherapp.model.WeatherModel;
import sda.code.weatherapp.retrofit.CityQuery;
import sda.code.weatherapp.retrofit.GeoQuery;
import sda.code.weatherapp.retrofit.OpenWeather;
import sda.code.weatherapp.retrofit.QueryBuilder;

import java.io.IOException;
import java.util.Optional;
import java.util.function.Consumer;

import static sda.code.weatherapp.Constants.API_KEY;

public class MainRetrofit {
    public static void main(String[] args) {
        System.out.println("Pogoda");

        Retrofit retrofitServiceFactory = new Retrofit.Builder()
                .baseUrl(Constants.OW_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Retrofit faultyServiceFactory = new Retrofit.Builder()
                .baseUrl(Constants.OW_BASE + ".abc")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        OpenWeather api = retrofitServiceFactory.create(OpenWeather.class);
        OpenWeather badApi = faultyServiceFactory.create(OpenWeather.class);

        GeoQuery geoQuery = new GeoQuery(35.011667, 135.768333);

        while (true) {
//            try {
//                geoQuery.execute(api, API_KEY).ifPresent(MainRetrofit::printForecast);
//                new CityQuery("Lodz").execute(api, API_KEY).ifPresent(MainRetrofit::printForecast);
//                new CityQuery("Pabianice").execute(api, API_KEY).ifPresent(MainRetrofit::printForecast);

//                sync(new QueryBuilder(api, API_KEY).withQuery(geoQuery).build())
//                        .ifPresent(MainRetrofit::printForecast);
//                sync(new QueryBuilder(api, API_KEY).withQuery(new CityQuery("Lodz")).build())
//                        .ifPresent(MainRetrofit::printForecast);
//                sync(new QueryBuilder(api, API_KEY)
//                        .withQuery(new CityQuery("Pabianice"))
//                        .withLangCode("fr")
//                        .withUnits("imperial")
//                        .build())
//                        .ifPresent(MainRetrofit::printForecast);

            async(new QueryBuilder(api, API_KEY).withQuery(geoQuery).build(),
                    MainRetrofit::printForecast);
            async(new QueryBuilder(api, API_KEY + "asda").withQuery(new CityQuery("Lodz")).build(),
                    MainRetrofit::printForecast);
            async(new QueryBuilder(api, API_KEY)
                            .withQuery(new CityQuery("Pabianice"))
                            .withLangCode("fr")
                            .withUnits("imperial")
                            .build(),
                    MainRetrofit::printForecast);
            async(new QueryBuilder(badApi, API_KEY).withQuery(new CityQuery("Lodz")).build(),
                    MainRetrofit::printForecast);

//            } catch (IOException e) {
//                System.err.println(e.getMessage());
//            }

            beIdle(3);
        }
    }

    private static <T> void async(Call<T> call, Consumer<T> consumer) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                if (response.isSuccessful()) {
                    consumer.accept(response.body());
                } else {
                    try {
                        System.err.println("Error: " + response.errorBody().string());
                    } catch (IOException e) {
                        System.err.println("Exception while reading an error body: " + e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                System.err.println("Exception: " + t.getMessage());
            }
        });
    }

    private static <T> Optional<T> sync(Call<T> call) throws IOException {
        Response<T> response = call.execute();
        if (response.isSuccessful()) {
            return Optional.ofNullable(response.body());
        } else {
            System.err.println(response.errorBody());
            return Optional.empty();
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Niezmienniki //////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static void printForecast(WeatherModel forecast) {
        System.out.print(forecast.getName());
        System.out.println(", " + forecast.getSys().getCountry());

        forecast.getWeather().forEach(w -> System.out.println(w.getDescription()));

        System.out.println(forecast.getMain().getTemp() + "℃");
    }

    private static void beIdle(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
