package sda.code.weatherapp;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import sda.code.weatherapp.model.WeatherModel;
import sda.code.weatherapp.retrofit.CityQuery;
import sda.code.weatherapp.retrofit.GeoQuery;
import sda.code.weatherapp.retrofit.Rx2OpenWeather;
import sda.code.weatherapp.retrofit.Rx2QueryBuilder;

import java.util.function.Consumer;

import static sda.code.weatherapp.Constants.API_KEY;

public class MainRx2Retrofit {
    public static void main(String[] args) {
        System.out.println("Pogoda");

        Retrofit retrofitServiceFactory = new Retrofit.Builder()
                .baseUrl(Constants.OW_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        Retrofit faultyServiceFactory = new Retrofit.Builder()
                .baseUrl(Constants.OW_BASE + ".abc")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        Rx2OpenWeather api = retrofitServiceFactory.create(Rx2OpenWeather.class);
        Rx2OpenWeather badApi = faultyServiceFactory.create(Rx2OpenWeather.class);

        GeoQuery geoQuery = new GeoQuery(35.011667, 135.768333);

        while (true) {
            async(new Rx2QueryBuilder(api, API_KEY).withQuery(geoQuery).build(),
                    MainRx2Retrofit::printForecast);
            async(new Rx2QueryBuilder(api, API_KEY + "asda").withQuery(new CityQuery("Lodz")).build(),
                    MainRx2Retrofit::printForecast);
            async(new Rx2QueryBuilder(api, API_KEY)
                            .withQuery(new CityQuery("Pabianice"))
                            .withLangCode("fr")
                            .withUnits("imperial")
                            .build(),
                    MainRx2Retrofit::printForecast);
            async(new Rx2QueryBuilder(badApi, API_KEY).withQuery(new CityQuery("Lodz")).build(),
                    MainRx2Retrofit::printForecast);

            beIdle(3);
        }
    }

    private static <T> void async(Observable<T> call, Consumer<T> consumer) {
        call
                .subscribeOn(Schedulers.io())

                // to wywołanie doOnNext wykona się na tym samym wątku, co zapytanie
                // ponieważ nie występuje przed nim żadne wywołanie observeOn
                .doOnNext(o -> System.out.println(Thread.currentThread().getName()))

//                .observeOn(Schedulers.newThread())
                .observeOn(Schedulers.computation())
//                .observeOn(Schedulers.immediate())

                // observer wykona się na wątku wskazanym w observeOn (albo subscribeOn, jeśli nie podamy observeOn)
                .subscribe(new Observer<T>() {
                    @Override
                    public void onError(Throwable e) {
                        System.err.println(Thread.currentThread().getName() + " Exception: " + e.getMessage() + ", " + e.getClass().getName());
                    }

                    @Override
                    public void onComplete() {
                        System.out.println(Thread.currentThread().getName() + " Done!");
                    }

                    @Override
                    public void onSubscribe(Disposable disposable) {
                        disposable.dispose();
                        System.out.println(Thread.currentThread().getName() + " onSubscribe: " + disposable);
                    }

                    @Override
                    public void onNext(T t) {
                        System.out.println(Thread.currentThread().getName());
                        consumer.accept(t);
                    }
                });
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//// Niezmienniki //////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private static void printForecast(WeatherModel forecast) {
        System.out.print(forecast.getName());
        System.out.println(", " + forecast.getSys().getCountry());

        forecast.getWeather().forEach(w -> System.out.println(w.getDescription()));

        System.out.println(forecast.getMain().getTemp() + "℃");
    }

    private static void beIdle(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
