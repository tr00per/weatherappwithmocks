package sda.code.weatherapp.retrofit;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;


@RunWith(JUnitParamsRunner.class)
public class QueryBuilderTest {
    private static final String DUMMY_KEY = "";

    private OpenWeather api;

    @Before
    public void setup() {
        api = mock(OpenWeather.class);
    }

    // Komentarz do podejścia: gdyby użyć interfejsu Refleksji zamiast lambd można by zyskać auto-dołączanie
    // nowych metod buildera oraz łatwiej uzyskać ładniejsze opisy testów (zamiast "QueryBuilderTest$$Lambda$1/1231231")
    @Test(expected = NullPointerException.class)
    @Parameters(method = "guardedMethods")
    public void checkMethodsAreGuardedAgainstNulls(Function<QueryBuilder, QueryBuilder> badInput) {
        badInput.apply(new QueryBuilder(api, DUMMY_KEY));
    }

    private List<Function<QueryBuilder, QueryBuilder>> guardedMethods() {
        return Arrays.asList(
                qb -> qb.withQuery((GeoQuery) null),
                qb -> qb.withQuery((CityQuery) null),
                qb -> qb.withUnits(null),
                qb -> qb.withLangCode(null)
        );
    }

    // Gdyby nie było lambd i testowania parametrycznego,
    // musielibyśmy wszystkie metody builder przetestować
    // osobnym kodem (odobnymi testami ze zduplikowaną logiką)
    @Test(expected = NullPointerException.class)
    public void checkWithUnitsAreGuardedAgainstNulls() {
        new QueryBuilder(api, DUMMY_KEY).withUnits(null);
    }

    @Test(expected = IllegalStateException.class)
    @Parameters(method = "verificationCases")
    public void validate(QueryBuilderMethod prepare) {
        prepare.apply(new QueryBuilder(api, DUMMY_KEY)).validate();
    }

    private QueryBuilderMethod[] verificationCases() {
        return new QueryBuilderMethod[]{
                qb -> qb,
                qb -> qb.withQuery(new CityQuery("abc")).withQuery(new GeoQuery(0.0, 0.0))
        };
    }

    @Test
    public void buildWithCityQuery() {
        new QueryBuilder(api, DUMMY_KEY).withQuery(new CityQuery("Lodz")).withLangCode("fr").withUnits("imperial").build();
        verify(api).weather(DUMMY_KEY, "Lodz", null, null, "imperial", "fr");
    }

    @Test
    public void buildWithGeoQueryAndDefaults() {
        new QueryBuilder(api, DUMMY_KEY).withQuery(new GeoQuery(1.0, 2.0)).build();
        verify(api).weather(DUMMY_KEY, null, 1.0, 2.0, "metric", "pl");
    }

    /**
     * Java disallows arrays of generics. This is a workaround.
     * Read more: https://www.ibm.com/developerworks/java/library/j-jtp01255/index.html
     */
    private interface QueryBuilderMethod
            extends Function<QueryBuilder, QueryBuilder> {
    }

}
