package sda.code.weatherapp;

import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.ListenableFuture;
import org.asynchttpclient.Request;
import org.asynchttpclient.Response;
import org.junit.Test;

import java.util.concurrent.CompletableFuture;

import static org.mockito.Mockito.*;
import static sda.code.weatherapp.TestConstants.*;

public class MainBardzoZlyTest {

    @Test
    public void showWeatherWhenServerOk() {
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        ListenableFuture listenable = mock(ListenableFuture.class);
        Response response = mock(Response.class);
        when(response.getStatusCode()).thenReturn(HTTP_OK);
        when(response.getResponseBody()).thenReturn(REAL_JSON);
        when(listenable.toCompletableFuture()).thenReturn(CompletableFuture.completedFuture(response));
        when(client.executeRequest(any(Request.class))).thenReturn(listenable);

        MainBardzoZly.client = client;

        MainBardzoZly.showWeather(Constants.BASE_URL, Constants.API_KEY, new CityQuery("Lodz")).join();
    }

    @Test
    public void showWeatherWhenServerFailed() {
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        ListenableFuture listenable = mock(ListenableFuture.class);
        Response response = mock(Response.class);
        when(response.getStatusCode()).thenReturn(HTTP_INTERNAL_SERVER_ERROR);
        verify(response, never()).getResponseBody();
        when(listenable.toCompletableFuture()).thenReturn(CompletableFuture.completedFuture(response));
        when(client.executeRequest(any(Request.class))).thenReturn(listenable);

        MainBardzoZly.client = client;

        MainBardzoZly.showWeather(Constants.BASE_URL, Constants.API_KEY, new CityQuery("Lodz")).join();
    }

    @Test
    public void showWeatherWhenResponseIsNull() {
        DefaultAsyncHttpClient client = mock(DefaultAsyncHttpClient.class);
        ListenableFuture listenable = mock(ListenableFuture.class);
        Response response = mock(Response.class);
        when(response.getStatusCode()).thenReturn(HTTP_OK);
        when(response.getResponseBody()).thenReturn(null);
        when(listenable.toCompletableFuture()).thenReturn(CompletableFuture.completedFuture(response));
        when(client.executeRequest(any(Request.class))).thenReturn(listenable);

        MainBardzoZly.client = client;

        MainBardzoZly.showWeather(Constants.BASE_URL, Constants.API_KEY, new CityQuery("Lodz")).join();
    }
}
