package sda.code.weatherapp;

public class TestConstants {
    public static final String REAL_JSON = "{\"coord\":{\"lon\":19.47,\"lat\":51.75},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"bezchmurnie\",\"icon\":\"01n\"}],\"base\":\"stations\",\"main\":{\"temp\":12,\"pressure\":1019,\"humidity\":100,\"temp_min\":12,\"temp_max\":12},\"visibility\":10000,\"wind\":{\"speed\":0.5},\"clouds\":{\"all\":0},\"dt\":1503262800,\"sys\":{\"type\":1,\"id\":5358,\"message\":0.0031,\"country\":\"PL\",\"sunrise\":1503200188,\"sunset\":1503251575},\"id\":3093133,\"name\":\"Lodz\",\"cod\":200}";
    public static final int HTTP_OK = 200;
    public static final int HTTP_INTERNAL_SERVER_ERROR = 500;

}
