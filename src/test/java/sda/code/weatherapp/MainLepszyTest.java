package sda.code.weatherapp;

import org.asynchttpclient.Response;
import org.junit.Test;

import java.util.concurrent.CompletableFuture;

import static org.mockito.Mockito.*;
import static sda.code.weatherapp.TestConstants.HTTP_INTERNAL_SERVER_ERROR;
import static sda.code.weatherapp.TestConstants.HTTP_OK;

public class MainLepszyTest {

    @Test
    public void showWeatherWhenServerOk() {
        MainLepszy.showWeather(Constants.BASE_URL, Constants.API_KEY, new CityQuery("Lodz"), r -> {
            Response response = mock(Response.class);
            when(response.getStatusCode()).thenReturn(HTTP_OK);
            when(response.getResponseBody()).thenReturn(TestConstants.REAL_JSON);
            return CompletableFuture.completedFuture(response);
        }).join();
    }

    @Test
    public void showWeatherWhenServerFailed() {
        MainLepszy.showWeather(Constants.BASE_URL, Constants.API_KEY, new CityQuery("Lodz"), r -> {
            Response response = mock(Response.class);
            when(response.getStatusCode()).thenReturn(HTTP_INTERNAL_SERVER_ERROR);
            verify(response, never()).getResponseBody();
            return CompletableFuture.completedFuture(response);
        }).join();
    }

    @Test
    public void showWeatherWhenResponseIsNull() {
        MainLepszy.showWeather(Constants.BASE_URL, Constants.API_KEY, new CityQuery("Lodz"), r -> {
            Response response = mock(Response.class);
            when(response.getStatusCode()).thenReturn(HTTP_OK);
            when(response.getResponseBody()).thenReturn(null);
            return CompletableFuture.completedFuture(response);
        }).join();
    }
}
